module Main (main) where

import        Data.List           (elemIndex)
import        Data.Maybe          (fromJust)
import        System.Environment  (getArgs)

enDeCrypt :: String -> String -> String -> Bool -> String
enDeCrypt phrase key alphabet encrypt =
  map ((!!) alphabet . getCharIndex) [0..length numbersPhrase - 1]
    where
      lenkey  = length key
      lenphr  = length phrase
      keyhash | lenkey == lenphr  = key
              | lenkey < lenphr   = let
                dpk = div lenphr lenkey
                mpk = mod lenphr lenkey
                kh  = [1..dpk] >>= const key
                  in if mpk == 0 then kh else kh ++ take mpk key
              | otherwise         = take lenphr key
      numbersPhrase   = map (fromJust . flip elemIndex alphabet) phrase
      numbersKeyhash  = map (fromJust . flip elemIndex alphabet) keyhash
      getCharIndex ch =
        let cr = numbersPhrase !! ch + numbersKeyhash !! ch * if encrypt then 1 else -1
        in if encrypt then
            if cr >= length alphabet then cr - length alphabet else cr
          else if cr < 0 then cr + length alphabet else cr

main :: IO ()
main = getArgs >>= \args -> case length args of
  4 -> let
      phrase        = args !! 1
      key           = args !! 2
      alphabet      = args !! 3
      isPhraseValid = all (`elem` alphabet) phrase
      isKeyValid    = all (`elem` alphabet) key
    in if isPhraseValid && isKeyValid then
        case head args of
          "encrypt" -> putStrLn $ enDeCrypt phrase key alphabet True
          "decrypt" -> putStrLn $ enDeCrypt phrase key alphabet False
          _ -> putStrLn "Usage: [encrypt|decrypt] [PHRASE] [KEY] [ALPHABET]" 
      else
        putStrLn "The phrase and the key must contain only characters from the alphabet"
  _ -> putStrLn "Usage: [encrypt|decrypt] [PHRASE] [KEY] [ALPHABET]"
